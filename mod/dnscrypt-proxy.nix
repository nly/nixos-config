## I have no idea about Nix definitions.
## So i've got this.
##
## External stateful dependencies:
##
##   1) umask 0027
##
##   2) git clone https://github.com/DNSCrypt/dnscrypt-proxy
##
##   3) see dnscrypt-proxy/README, and https://dnscrypt.info/doc.
##
##   4) cd dnscrypt-proxy/dnscrypt-proxy;  export CGO_ENABLED=0
##
##   5) mkdir -p /.dns/examples;  cp example-* /.dns/examples
##
##   5) go build -mod vendor;  install -m550 dnscrypt-proxy /.dns
##
##   6) mkdir -m770 /.dns/rw    # it is for public-resolvers.md
##
##   7) cp /.dns/examples/example-dnscrypt-proxy.toml /.dns/dnscrypt-proxy.toml
##
##   8) edit /.dns/dnscrypt-proxy.toml, but try run dnscrypt as is first.
##
##   8) chown -R root:dnscrypt /.dns
##        maybe after nixos-rebuild, as group may not exist yet.
##
##   9) watch for updates with `git pull`, repeat starting from 4).

{ config, pkgs, ... }:

{
  users.groups = { dnscrypt = {}; };

  networking =
    { firewall =
        { allowedUDPPorts = [ 53 ];
          allowedTCPPorts = [ 53 ];
        };
      resolvconf.useLocalResolver = true;
    };

  ## The sandboxing can be improved i think.
  ## See `systemd-analyze security dnscrypt`.
  ##
  systemd.services =
    { dnscryptproxy2 =
        { enable = true;
          after = [ "network.target" ];
          wantedBy = [ "network.target" ];

          serviceConfig =
            { DynamicUser = "true";
              Group = "dnscrypt";
              WorkingDirectory = "/.dns";
	      BindPaths = "/.dns/rw";
              UMask = "0077";

              ExecStart = "/.dns/dnscrypt-proxy"; 

              AmbientCapabilities = "CAP_NET_BIND_SERVICE";
              CapabilityBoundingSet = "CAP_NET_BIND_SERVICE";
              LockPersonality = "true";
              MemoryDenyWriteExecute = "true";
              SystemCallArchitectures = "native";

              ProtectProc = "invisible";
              ProcSubset = "pid";

	      PrivateDevices = "true";

              ProtectClock = "true";
              ProtectControlGroups = "true";
              ProtectHome = "true";
	      ProtectHostname = "true";
              ProtectKernelLogs = "true";
              ProtectKernelModules = "true";
              ProtectKernelTunables = "true";
              RestrictAddressFamilies = "AF_INET AF_INET6 AF_NETLINK AF_PACKET";
              RestrictNamespaces = "true";
              RestrictRealtime = "true";

              SystemCallFilter = "~ @cpu-emulation @debug @mount @obsolete @privileged @resources";
            };
        };
    };
}
