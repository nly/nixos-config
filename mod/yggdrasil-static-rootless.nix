## External stateful dependencies:
##
##   1) sudo -i
##
##   2) umask 0027        # config has a private key
##
##   3) mkdir /etc/yggdrasil;  cd /etc/yggdrasil
##
##   4) nix-shell -p yggdrasil --command 'yggdrasil -genconf' > config
##
##   5) sed -i 's,\(AdminListen: *\).*,\1none,' config
##      set (in config) the LinkLocalTCPPort to 65535 (see below)
##
##   6) see  https://github.com/yggdrasil-network/public-peers
##      You may want to add some peers to your config to get more than LAN.
##      Preferably those that are geographically closer to you.
##
##   7) include this .nix file to configuration.nix
##
##   8) nixos-rebuild switch        # will error, it's ok
##
##   9) chown -R root:ygg /etc/yggdrasil
##
##   10) systemctl restart yggdrasil
##
##
##   You can connect one device to Yggdrasil, and connect others to it, and
##     still access the Yggdrasil network.  But it may be slower than if all
##     devices were independent nodes with some external (non LAN) peers.
##
##   Not sure about netiqette, asked on forum some days ago.

{ config, pkgs, ... }:

{
  users.groups = { ygg = {}; };

  ## Incoming peering.  The Public Peers may experience high CPU load.
  ## I think it is a LAN multicast port, though.
  networking.firewall =
    { allowedUDPPorts = [ 9001 ];
      allowedTCPPorts = [ 65535 ];
    };

  ## The sandboxing can be improved i think.
  ## See `systemd-analyze security yggdrasil`.
  ##
  systemd.services =
    { yggdrasil =
        { enable = true;
          after = [ "network.target" ];
          wantedBy = [ "network.target" ];

          serviceConfig =
            { DynamicUser = "true";
              Group = "ygg";
              WorkingDirectory = "/etc/yggdrasil";
              UMask = "0077";

              ExecStart = "/${pkgs.yggdrasil}/bin/yggdrasil --useconffile config";

              AmbientCapabilities = "CAP_NET_ADMIN";
              CapabilityBoundingSet = "CAP_NET_ADMIN";
              LockPersonality = "true";
              MemoryDenyWriteExecute = "true";
              SystemCallArchitectures = "native";

              ProtectProc = "invisible";
              ProcSubset = "pid";

              DevicePolicy = "closed";
              DeviceAllow = "/dev/net/tun";

              ProtectClock = "true";
              ProtectControlGroups = "true";
              ProtectHome = "true";
              ProtectKernelLogs = "true";
              ProtectKernelModules = "true";
              ProtectKernelTunables = "true";
              RestrictAddressFamilies = "AF_INET AF_INET6 AF_NETLINK AF_PACKET";
              RestrictNamespaces = "true";
              RestrictRealtime = "true";

              SystemCallFilter = "~ @cpu-emulation @debug @mount @obsolete @privileged @resources";
            };
        };
    };
}
