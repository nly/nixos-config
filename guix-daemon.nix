## See GNU Guix official site, get the official installation script.
## Runt fearlessly on NixOS, and then add this file to your config.
## In June of 2021 the settings below are current.

{ lib, ... }:

{
  systemd =
    { mounts =
        [
          { enable = true;
            before = [ "guix-daemon.service" ];
            description = "Read-only /gnu/store for GNU Guix";
            options = "bind,ro";
            wantedBy = [ "guix-daemon.service" ];
            what = "/gnu/store";
            where = "/gnu/store";
          }
        ];
      services.guix-daemon =
        { enable = true;
          description = "Build daemon for GNU Guix";
          after = [ "gnu-store.mount" ];
          serviceConfig =
            { ExecStart = "/var/guix/profiles/per-user/root/current-guix/bin/guix-daemon --build-users-group=guixbuild";
              Environment = "GUIX_LOCPATH=/var/guix/profiles/per-user/root/guix-profile/lib/locale LC_ALL=en_US.utf8";
              RemainAfterExit = "yes";
              StandardOutput = "syslog";
              StandardError = "syslog";
              TasksMax = "8192";
            };
          wantedBy = [ "multi-user.target" ];
        };
    };
}
