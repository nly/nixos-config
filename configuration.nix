# Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ config, pkgs, ... }:

{
  imports =
    [ #./mod/dnscrypt-proxy.nix
      ./mod/hardware-configuration.nix
      ./mod/yggdrasil-static-rootless.nix
      ./guix-daemon.nix
    ];
  sound.enable = true;
  hardware.pulseaudio.enable = true;

  # Use the systemd-boot EFI boot loader.
  boot =
    { loader =
        { systemd-boot.enable = true;
          efi.canTouchEfiVariables = false;
        };
      supportedFilesystems = [ "zfs" ];
    };

  #docker
  virtualisation.docker.enable = true;

  services.xserver.enable = true;
  services.xserver.displayManager.gdm.enable = true;
  services.xserver.desktopManager.gnome.enable = true;
  services.xserver.videoDrivers = [ "amdgpu" ];

  networking = {
    hostId = "818f063f";                     # hex; for ZFS; taken from uuidgen.
    hostName = "uf";

    networkmanager.enable = false;
    wireless.enable = true;
    wireless.userControlled.enable = true;
    wireless.networks.FFF.pskRaw = "27ade8312b6c980ae36a699af5a33a1e6746d9c09cde907a4727c0633b2960b6";
    wireless.interfaces = ["wlp1s0"];
    interfaces.wlp1s0.useDHCP = true;

    firewall = {
      enable = true;
      extraCommands = "
iptables        -A INPUT      -p tcp --dport 9040     -j ACCEPT
iptables -t nat -A PREROUTING -p tcp -d 127.192.0.0/11 -j REDIRECT --to-port 9040
iptables -t nat -A OUTPUT -p tcp -d 127.192.0.0/11 -j REDIRECT --to-port 9040

iptables -t nat -A OUTPUT -d 10.191.0.1 -p tcp -j REDIRECT --to-port 4444
iptables -t nat -A PREROUTING -d 10.191.0.1 -p tcp -j REDIRECT --to-port 4444

iptables -t nat -A OUTPUT -d 10.191.0.1 -p tcp --dport 80 -j REDIRECT --to-port 8118
iptables -t nat -A PREROUTING -d 10.191.0.1 -p tcp --dport 80 -j REDIRECT --to-port 8118";
      allowedTCPPorts = [ 22 80 443 8080 3000];
      allowedUDPPorts = [ 22 80 443 8080 3000];
    };
 };

  time.timeZone = "UTC";

  console = {
    font = "latarcyrheb-sun16";
    keyMap = "dvorak";
  };

  users.groups.nly.gid = 1024;

  users.users = {
    nly = {
      group = "nly";
      extraGroups = [ "wheel" "video" "audio" "docker"];
      isNormalUser = true;
      uid = 1001;
    };
  };

  security.pam.services.su.requireWheel = true;

  environment = {
    systemPackages = with pkgs; [
      # apps
      emacs git wget bind curl darkhttpd
    ];
    loginShellInit = "
init_guix () {
        # system's guix, user's guix, and user's profile
        local s u p
        s=/var/guix/profiles/per-user/root/current-guix/etc/profile
        u=~/.config/guix/current/etc/profile
        p=~/.guix-profile/etc/profile

        if [ -f $u ]; then source $u; else source $s; fi
        if [ -f $p ]; then source $p; fi
}; init_guix";
  };
  services.flatpak.enable = true;
  services = {
    openssh = {
      enable = true;
      allowSFTP = true;
      challengeResponseAuthentication = false;        # what's it?
      forwardX11 = false;                             # not secure?
      permitRootLogin = "prohibit-password";
      passwordAuthentication = false;
    };
    i2pd = {
      enable = true;                               # no extra config
      proto.httpProxy.enable = true;
    };
    tor = {
      enable = true;
      settings = {
        DNSPort = 5353;
        TransPort = 9040; # ["IsolateClientAddr" "IsolateClientProtocol" "IsolateDestAddr" "IsolateDestPort"];

        AutomapHostsOnResolve = true;
        VirtualAddrNetworkIPv4 = "127.192.0.0/11";
        VirtualAddrNetworkIPv6 = "[FE80::]/10";

        # Hidden Service SSH
        HiddenServiceDir = "/var/lib/tor/openssh/";
        HiddenServicePort = 22;

      };
    };
    unbound = {
      enable = true;
      settings = {
        server = {
          domain-insecure = "onion";
          private-domain = "onion";
          do-not-query-localhost = "no";
          local-zone = [ "onion. nodefault" "\"i2p\" redirect"];
          local-data = "\"i2p A 10.191.0.1\"";
        };
        forward-zone = [
          { name = "onion";
            forward-addr = "127.0.0.1@5353";
            forward-first = "no";
          }
          { name = "ygg";
            ## this is a rather funny construction: some 4 real DNS resolvers
            ##  (ALFIS) behind an /etc/hosts-on-steroids resolver.
            forward-addr = "127.0.0.2@8053";
            forward-first = "no";
          }
        ];
      };
    };
    privoxy = {
      enable = true;
      settings = {
        accept-intercepted-requests = true;
        forward = ".i2p 127.0.0.1:4444";
      };
    };
  };

  # Before changing this value read the documentation for this option
  # (e.g. man configuration.nix or on https://nixos.org/nixos/options.html).
  system.stateVersion = "21.03"; # Did you read the comment?

}

